import { Component} from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HistoryService } from '../api/history.service';
import { HistoryRecord } from '../models/history-record.model';



@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  leagues:String;
  historyArray: HistoryRecord[];

  
  constructor(public navCtrl: NavController, 
              private router: Router,
              private historyService: HistoryService) {
    
      
    
  }
  ionViewWillEnter()
  {
    this.historyArray = this.historyService.getRecord();

  }

  ngOnInit(){
    this.leagues="";
  }
  public showSelectedLeague(): void
  {
    console.log("Select:"+this.leagues);
    this.router.navigate([`tabs/tab2/${this.leagues}`]);
    
  }
  
  
}
