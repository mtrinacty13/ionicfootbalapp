import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import { FootballdataService } from '../api/footballdata.service';
import { LoadingService } from '../api/loading.service';
import { HistoryRecord } from '../models/history-record.model';
import { HistoryService } from '../api/history.service';



@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  leagues: String;

  leagueID: String;
  loadingDialog: any;
  response: any;
  historyArray: HistoryRecord[];
  constructor(public route: ActivatedRoute,
    private apiService: FootballdataService,
    public loadCtrl: LoadingService,
    private storage: Storage,
    private historyService: HistoryService) {


    this.leagues = this.route.snapshot.params.id;
    console.log("from user:" + this.leagues);

    if (this.leagues === undefined) {
      this.historyArray = this.historyService.getRecord();
      this.leagues = this.historyArray[0].leagueID;
      console.log("from history:" + this.leagues);
    }
  }
  ionViewWillEnter() {
    this.loadCtrl.present();
    console.log("tab2" + this.leagues);
    this.leagueID = this.leagues;
    this.apiService.getStandings(this.leagues).subscribe((data => {
      this.leagues = data['competition']['name'];
      this.response = data['standings'][0]['table'];
      console.log(this.response);
      let record = new HistoryRecord(this.leagueID, this.leagues);
      this.historyService.saveRecord(record);
      this.loadCtrl.dismiss();
    }));

  }

}
