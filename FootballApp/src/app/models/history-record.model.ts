export class HistoryRecord 
{
    leagueID: String
    leagueName: String

    constructor(leagueID: String, leagueName: String) 
    {
        this.leagueID = leagueID;
        this.leagueName = leagueName;
    }
}