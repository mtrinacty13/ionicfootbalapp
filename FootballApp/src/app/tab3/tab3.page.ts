import { Component } from '@angular/core';
import { FootballdataService } from '../api/footballdata.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  matches:string;
  time:string;
  response:any;
  constructor(private apiService: FootballdataService) {}

  public showSelectedMatches(): void
  {

    this.apiService.getMatches(this.matches).subscribe(data=>{
      console.log(data['matches']);
      
      this.response=data['matches'];
    });
    
  }
}
