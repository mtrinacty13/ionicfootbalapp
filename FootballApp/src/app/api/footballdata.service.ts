import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FootballdataService {
  webReturn: any;
  authToken: string;
  auth: string;
  basicUrl: string;
  constructor(private http: HttpClient) {
    this.authToken = "1f0caabbfd784b1cbbb4bce1b7c29165";
    this.auth="X-Auth-Token";
    this.basicUrl="https://api.football-data.org/v2/";
   }
  

  public getStandings(leagueID:String) {
    let url = this.basicUrl+"competitions/"+leagueID+"/standings";
    

    
    const  headers= new HttpHeaders().set(this.auth,this.authToken)
    

     return this.http.get(url, { headers });
      
  }
  public getMatches(matches:String) {
  
    let url = this.basicUrl+"matches?status="+matches;
    //let url =  this.basicUrl+"teams/86/matches?status="+matches;

    
    const  headers= new HttpHeaders().set(this.auth,this.authToken)
    

     return this.http.get(url, { headers });
      
  }
}

